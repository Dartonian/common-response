package response

import (
	"bytes"
	"encoding/json"
	"fmt"
	"html"
	"log"
	"net/http"
)

// ErrorHandler for http
func ErrorHandler(w http.ResponseWriter, err error) {
	if err != nil {
		log.Printf("Error during getting item: %s.", err)
		JSONErrorResponse(w, err, http.StatusInternalServerError)
		return
	}
}

// JSONErrorResponse for http
func JSONErrorResponse(w http.ResponseWriter, err error, statusCode int) {
	JSONResponse(w, map[string]string{"message": err.Error()}, statusCode)
}

// JSONResponse for http
func JSONResponse(w http.ResponseWriter, data interface{}, statusCode int) {
	w.Header().Set("Content-Type", "application/json")

	fmt.Println(data)

	var buf bytes.Buffer

	enc := json.NewEncoder(&buf)
	enc.SetEscapeHTML(false)
	enc.Encode(data)

	w.WriteHeader(statusCode)
	fmt.Fprintf(w, html.UnescapeString(buf.String()))
}
